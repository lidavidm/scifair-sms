# Errata

## Data Table

* should read "continuation\_in\_reverse", not "detection\_in\_reverse"

## Data Analysis

* should be "440-Hertz sine wave", not "600-Hertz"
