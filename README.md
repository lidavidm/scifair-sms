# SMSPy (Python Spectral Modeling Synthesis)

## Introduction

This implements sinusoidal synthesis as described in X. Serra's PhD thesis
"A System for Sound Analysis/Transformation/Synthesis Based on a
Deterministic Plus Stochastic Decomposition" (this project is misnamed) for
a science fair project.

## Installation and Requirements

This project requires Python 3 in addition to NumPy and SciPy. There is no
installation procedure since the library is not in a state ready for use
outside of this experiment.

## Usage

To run the experiment, run (from the project directory):

    $ cd experiment
    $ python3 makeinputs.py
    $ cd ..
    $ python3 experiment.py
    
The resulting synthesized sounds can then be found in a directory under the
`experiment/outputs` directory, based on the optimizations used during the
synthesis.

## Building the Research Report

Requirements:

* ConTeXt MkIV
* Gnuplot
* The Gentium Book Basic, AMS Euler, and DejaVu Sans Mono fonts.

To create the report, execute:

    $ cd report
    $ context report.tex
