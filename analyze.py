#!/usr/bin/env python3
import argparse
import scipy.io.wavfile
import numpy
import pickle
import math

import smspy
from smspy import ioutils
from smspy.stft import STFT, istft
from smspy.peakdetection import detectPeaks, Peak
from smspy.peakcontinuation import peakContinuation, Guide
from smspy.synthesis import synthesize

def peaks(stftfile, output, **kwargs):
    stft = STFT.load(stftfile)
    fs = stft.fs
    peaks = map(lambda frame: map(lambda peak: peak.toTuple(), frame),
                detectPeaks(stft, fs, min_peak_height=40))
    ioutils.savePeaks(peaks, output)

def precompute(audiofile, output, **kwargs):
    fs, data = scipy.io.wavfile.read(audiofile)
    print('Sampling Frequency:', fs)
    stft = STFT(data / numpy.iinfo(numpy.int16).max, fs)
    stft.compute(numpy.hamming, 0.050, 0.020)
    stft.save(output)

def continuation(peakfile, output, **kwargs):
    frames = ioutils.loadPeaks(peakfile)
    # frequency distance needs to be at least the same as frequency
    # resolution of STFT
    trajs = list(peakContinuation(frames, [],
                                  1000,#0.261799388,
                                  1))
    print("Number of trajectories:", len(trajs))
    pickle.dump(trajs, open(output, 'wb'))

def synthesis(trajfile, stftfile, output, **kwargs):
    stft = STFT.load(stftfile)
    trajs = pickle.load(open(trajfile, 'rb'))

    ff, fs, n = stft.ff, int(stft.fs), int(stft.size)
    T = stft.audioFrames / fs  # get time length
    print("Synthesizing", T, "seconds of audio at", fs, "Hz,",
          "fundamental frequency", ff, "FFT size", n)

    frames = synthesize(trajs, stft)

    aMax = max(frames)
    dMax = numpy.iinfo(numpy.int16).max

    audio = numpy.fromiter((int(x / aMax * dMax) for x in frames),
                           numpy.int16, len(frames))
    print(audio)
    scipy.io.wavfile.write(output, fs, audio)

# def synthesis(trajfile, stftfile, output, **kwargs):
#     stft = STFT.load(stftfile)
#     ff, fs, n = stft.ff, int(stft.fs), int(stft.size)
#     T = stft.audioFrames / 44100  # get time length
#     print("Synthesizing", T, "seconds of audio at", fs, "Hz,",
#           "fundamental frequency", ff, "FFT size", n)
#     frames = []
#     def padList(lst, index):
#         if index > len(lst):
#             for i in range(index - len(lst)):
#                 lst.append([0] * n)
#     for tNo, traj in enumerate(pickle.load(open(trajfile, 'rb'))):
#         print("Synthesizing trajectory", tNo)
#         print(traj.peaks[1].frequency)
#         padList(frames, traj.startframe + len(traj.peaks))
#         for pIndex, (peak, prevPeak) in enumerate(
#             zip(
#                 traj.peaks,
#                 [Peak.zero()] + traj.peaks[1:]
#                 )
#             ):
#             frame = frames[traj.startframe + pIndex]
#             for m, (mag, phase) in enumerate(zip(
#                 interpolateMag(n, prevPeak, peak),
#                 interpolatePhase3(n, prevPeak, peak)
#                 )):
#                 #print(phase, math.cos(phase), mag*math.cos(phase))
#                 frame[m] += mag * math.cos(phase)

#     aMax = max(map(max, frames))
#     dMax = numpy.iinfo(numpy.int16).max
#     audio = numpy.fromiter(
#         (int((x / aMax) * dMax) for f in frames for x in f),
#         numpy.int16, count=sum(len(x) for x in frames)
#         )
#     print(audio)
#     scipy.io.wavfile.write(output, fs, audio)

def interpolateMag(n, prevPeak, peak):
    deltaMagnitude = (peak.magnitude - prevPeak.magnitude) / n
    for m in range(n):
        yield prevPeak.magnitude + (deltaMagnitude * m)

def interpolatePhase3(n, prevPeak, peak):
    deltaFreq = (peak.frequency - prevPeak.frequency) / n
    freq = prevPeak.frequency
    for m in range(n):
        freq += deltaFreq * m
        yield (freq * m)

def interpolatePhase(n, prevPeak, peak):
    deltaFreq = (peak.frequency - prevPeak.frequency) / n
    freq = prevPeak.frequency
    for m in range(n):
        freq += deltaFreq * m
        yield prevPeak.phase + (freq * m)

def interpolatePhase2(n, prevPeak, peak):
    curPhase = peak.phase
    prevPhase = prevPeak.phase
    curFreq = peak.frequency
    prevFreq = prevPeak.frequency
    THREE_OVER_S2 = 3 / (n ** 2)
    NTWO_OVER_S3 = -2 / (n ** 3)
    ONE_OVER_S = 1 / n
    ONE_OVER_S2 = 1 / (n ** 2)
    C1 = curPhase - prevPhase - (prevFreq * n)
    deltaFreq = curFreq - prevFreq

    def eta(m):
        return ((((2 * math.pi * m) + C1) * THREE_OVER_S2) -
                (ONE_OVER_S * deltaFreq))

    def iota(m):
        return ((((2 * math.pi * m) + C1) * NTWO_OVER_S3) +
                (ONE_OVER_S2 * deltaFreq))

    for m in range(n):
        yield (prevPhase + (prevFreq * m) +
               (eta(m) * m ** 2) +
               (iota(m) * m ** 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='Commands: precompute peaks')
    precomputeParser = subparsers.add_parser('precompute')
    precomputeParser.add_argument('audiofile')
    precomputeParser.add_argument('output')
    precomputeParser.set_defaults(func=precompute)
    peakParser = subparsers.add_parser('peaks')
    peakParser.add_argument('stftfile')
    peakParser.add_argument('output')
    peakParser.set_defaults(func=peaks)
    continuationParser = subparsers.add_parser('continuation')
    continuationParser.add_argument('peakfile')
    continuationParser.add_argument('output')
    continuationParser.set_defaults(func=continuation)
    synthParser = subparsers.add_parser('synthesis')
    synthParser.add_argument('trajfile')
    synthParser.add_argument('stftfile')
    synthParser.add_argument('output')
    synthParser.set_defaults(func=synthesis)
    args = parser.parse_args()
    args.func(**vars(args))
