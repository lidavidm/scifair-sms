#!/usr/bin/env python3
import os
import csv
import math
import glob
import numpy
import pickle
import collections
import scipy.io.wavfile

from itertools import chain, combinations

import smspy
from smspy import ioutils
from smspy.util import mag2dB
from smspy.stft import STFT, istft
from smspy.peakdetection import detectPeaks, Peak
from smspy.peakcontinuation import peakContinuation, Guide
from smspy.synthesis import synthesize

OUTPUT_DIR = './experiment/outputs'
INPUT_FILES = glob.glob('./experiment/inputs/*.wav')
TEMP = './experiment/temp'

class option:
    options = {}
    enabled_options = {}
    def __init__(self, option):
        self.option = option

    def __call__(self, func):
        func.option = self.option
        self.__class__.options[self.option] = func
        self.__class__.enabled_options[self.option] = False

        def _funcwrapper(*args, **kwargs):
            if self.__class__.enabled_options[self.option]:
                if self.option not in kwargs:
                    kwargs[self.option] = True
            return func(*args, **kwargs)

        return _funcwrapper


@option('zero_pad')
def computeSTFT(audio, fs, zero_pad=False):
    stft = STFT(data / numpy.iinfo(numpy.int16).max, fs)
    stft.compute(numpy.hamming, 0.050, 0.020, padding=zero_pad)
    return stft


@option('interpolate_detected_peaks')
def findPeaks(stft, interpolate_detected_peaks=False):
    return list(detectPeaks(
        stft, stft.fs,
        min_peak_height=40,
        interpolate_detected_peaks=interpolate_detected_peaks))


@option('detect_in_reverse')
def findTrajectories(peaks, detect_in_reverse=False):
    return list(peakContinuation(peaks, [], 1000, 1,
                                 reverse_detection=detect_in_reverse))


def synthesizeOutput(stft, trajectories):
    frames = synthesize(trajs, stft)

    aMax = max(frames)
    dMax = numpy.iinfo(numpy.int16).max

    audio = numpy.fromiter((int(x / aMax * dMax) for x in frames),
                           numpy.int16, len(frames))

    return audio


def averageFFT(size, audio, window):
    window = window(size)

    ffts = []
    for start in range(0, len(audio), size):
        data = audio[start:start + size]
        if len(data) < size:
            data = data.copy()
            data.resize(size)  # zero-pad it
        res = numpy.fft.fft(window * data, size)
        ffts.append(res)

    numFFTs = len(ffts)
    return numpy.fromiter(
        (sum(fft[current] / numFFTs for fft in ffts) for current in range(size)),
        dtype=numpy.complex,
        count=size
    )


def evaluateOutput(output, original):
    outputFFT = averageFFT(1024, output, numpy.hamming)
    originalFFT = averageFFT(1024, original, numpy.hamming)
    comparison = numpy.fromiter(
        (out - orig for out, orig in zip(outputFFT, originalFFT)),
        dtype=numpy.complex, count=len(outputFFT))
    return outputFFT, originalFFT, comparison


def writeCSVOutput(f, ff, data):
    writer = csv.writer(f)
    writer.writerow(("Frequency", "Magnitude"))
    for index, val in enumerate(data):
        mag = mag2dB(numpy.linalg.norm(val))
        freq = index * ff
        writer.writerow((freq, mag))


def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))


if __name__ == '__main__':
    try:
        os.makedirs(OUTPUT_DIR)
    except OSError:
        pass

    try:
        os.makedirs(TEMP)
    except OSError:
        pass

    errors = collections.defaultdict(dict)

    for combination in powerset(option.options.keys()):
        print("Enabled options:", ' '.join(combination))
        #if input('Skip? (skip if non-empty) ').strip():
        #    continue

        # Clear and set options
        for opt in option.enabled_options:
            if opt not in combination:
                option.enabled_options[opt] = False
            else:
                option.enabled_options[opt] = True

        DIR = 'options' + '__'.join(combination)

        try:
            os.makedirs(os.path.join(TEMP, DIR))
        except OSError:
            pass

        try:
            os.makedirs(os.path.join(OUTPUT_DIR, DIR))
        except OSError:
            pass

        for inputwav in INPUT_FILES:
            root, _ = os.path.splitext(os.path.basename(inputwav))
            print("Analyzing", inputwav)

            fs, data = scipy.io.wavfile.read(inputwav)
            stft = computeSTFT(data, fs)
            stft.save(os.path.join(TEMP, DIR, root + '.stft'))
            print("\tComputed STFT")

            peaks = findPeaks(stft)
            ioutils.savePeaks(peaks, os.path.join(TEMP, DIR, root + '.peaks'))
            print("\tComputed peaks")

            trajs = findTrajectories(peaks)
            pickle.dump(trajs,
                        open(os.path.join(TEMP, DIR, root + '.trajs'), 'wb'))
            print("\tFound trajectories")

            output = synthesizeOutput(stft, trajs)
            scipy.io.wavfile.write(os.path.join(OUTPUT_DIR, DIR, root + '.wav'),
                                   fs, output)
            print("\tWrote output")

            output, original, comparison = evaluateOutput(output, data)

            makePath = lambda name: os.path.join(
                OUTPUT_DIR, DIR, root + '_' + name + '.csv')

            with open(makePath('output'), 'w') as f:
                writeCSVOutput(f, fs / 1024, output)

            with open(makePath('original'), 'w') as f:
                writeCSVOutput(f, fs / 1024, original)

            with open(makePath('comparison'), 'w') as f:
                writeCSVOutput(f, fs / 1024, comparison)

            rmse = math.sqrt(sum(
                numpy.linalg.norm(x) ** 2 for x in comparison
                ) / len(comparison))

            errors[combination][root] = rmse
            print(errors)

            print("\tWrote analysis")

    with open(os.path.join(OUTPUT_DIR, 'rmse.csv'), 'w') as f:
        writer = csv.writer(f)
        writer.writerow(["Optimizations", "Input Sound", "RMSE (dB)"])
        for opts, data in sorted(errors.items()):
            row = sorted(data.items())
            item = row[0]
            writer.writerow([', '.join(opts), item[0], item[1]])
            total = item[1]
            number = 1
            for sound, rmse in row[1:]:
                writer.writerow(['', sound, rmse])
                total += rmse
                number += 1
            writer.writerow(['', 'Average', total / number])