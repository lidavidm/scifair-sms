#!/usr/bin/env python3
import csv
import sys
import pickle
import argparse
import itertools

sys.path.append('..')  # get the SMSPy library in the search path

import smspy
from smspy import ioutils
from smspy.util import mag2dB
from smspy.stft import STFT


def stft(stft, frame, output, **options):
    stft = STFT.load(stft)
    signal = stft._audioData[frame:frame + int(0.050 * stft._fs)]
    spectrum = stft.dBSpectra[frame]

    with open(output, 'w') as f:
        csvoutput = csv.writer(f)
        for i, (signal, mag) in enumerate(
                itertools.zip_longest(signal, spectrum, fillvalue=None)):
            row = []
            if signal:
                row.append(i)
                row.append(signal)
            if mag:
                row.append(i * stft.ff)
                row.append(mag)
            csvoutput.writerow(row)



def peakDetection(stft, peaks, frame, output, **options):
    peaks = ioutils.loadPeaks(peaks)[frame]
    stft = STFT.load(stft)
    magScale = 2 / stft._windowTransform
    magSpectrum = stft.dBSpectra[frame]
    print("Number of peaks:", len(peaks))
    print("Length of spectrum:", len(magSpectrum))
    print(peaks)
    with open(output, 'w') as f:
        csvoutput = csv.writer(f)
        for ((fIndex, mag),
             peak) in itertools.zip_longest(
                 enumerate(magSpectrum),
                 peaks,
                 fillvalue=None
             ):
            row = [fIndex * stft.ff, mag, '', '']
            if peak:
                row[2:4] = [peak.frequency, mag2dB(peak.magnitude)]
            csvoutput.writerow(row)
    print("Wrote output")


def peakContinuation(peaks, trajs, framel, frameu, output, **options):
    peaks = ioutils.loadPeaks(peaks)[framel:frameu]
    trajs = pickle.load(open(trajs, 'rb'))
    present = []
    for traj in trajs:
        if (traj.startframe >= framel and
            (traj.startframe + len(traj.peaks)) < frameu):
            present.append(traj)
    rows = []
    maxMagnitude = max(peak.magnitude for f in peaks for peak in f)

    for frame, fpeaks in enumerate(peaks):
        for peak in fpeaks:
            ptsize = (peak.magnitude / maxMagnitude) * 2
            if ptsize < 1:
                ptsize = 1
            rows.append([frame, peak.frequency, ptsize])

    with open(output, 'w') as f:
        csvoutput = csv.writer(f)
        csvoutput.writerows(rows)

    for trajNo, traj in enumerate(trajs):
        trajrows = []
        for i, peak in enumerate(traj.peaks):
            if traj.startframe + i >= frameu:
                break
            trajrows.append([traj.startframe + i, peak.frequency])

        if trajrows:
            with open('traj_' + str(trajNo) + '.csv', 'w') as f:
                csvoutput = csv.writer(f)
                csvoutput.writerows(trajrows)





if __name__ == '__main__':
    mainParser = argparse.ArgumentParser()
    subparsers = mainParser.add_subparsers()

    stftParser = subparsers.add_parser('stft')
    stftParser.add_argument('stft')
    stftParser.add_argument('frame', type=int)
    stftParser.add_argument('output')
    stftParser.set_defaults(func=stft)

    pdParser = subparsers.add_parser('peak_detection')
    pdParser.add_argument('stft')
    pdParser.add_argument('peaks')
    pdParser.add_argument('frame', type=int)
    pdParser.add_argument('output')
    pdParser.set_defaults(func=peakDetection)

    pcParser = subparsers.add_parser('peak_continuation')
    pcParser.add_argument('peaks')
    pcParser.add_argument('trajs')
    pcParser.add_argument('framel', type=int)
    pcParser.add_argument('frameu', type=int)
    pcParser.add_argument('output')
    pcParser.set_defaults(func=peakContinuation)

    args = mainParser.parse_args()
    args.func(**vars(args))