#!/usr/bin/env python3
import os
import itertools

import numpy
import scipy.io.wavfile as wavfile

OUTPUT = './inputs/'
frequencies = [440, 600]
fs = 8000
T = 3

dmax = numpy.iinfo(numpy.int16).max

def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return itertools.chain.from_iterable(
        itertools.combinations(s, r) for r in range(len(s)+1))

if __name__ == '__main__':
    waveforms = []
    t = numpy.linspace(0, T, T*fs, endpoint=False)

    for frequency in frequencies:
        waveforms.append(('sin' + str(frequency),
                          numpy.sin(2*numpy.pi*frequency*t)))
        #waveforms.append(('cos' + str(frequency),
        #                  numpy.cos(2*numpy.pi*frequency*t)))

    for waves in powerset(waveforms):
        if waves:  #discard the empty set
            path = '_'.join(wave[0] for wave in waves)
            path = os.path.join(OUTPUT, path + '.wav')
            audio = sum(wave[1] for wave in waves)
            audio = audio / audio.max() * dmax
            wavfile.write(path, fs, audio.astype(numpy.int16))
