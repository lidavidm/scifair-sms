#!/usr/bin/env python3
import sys
import numpy


def main():
    npz = numpy.load(sys.argv[1])
    print("Sampling Frequency:", npz['header'][0])
    numTrajs = npz['header'][1]
    print("Number of trajectories:", numTrajs)
    for i in range(numTrajs):
        head = npz['trajhead{}'.format(i)]
        mags = npz['trajmags{}'.format(i)]
        frqs = npz['trajfrqs{}'.format(i)]
        phss = npz['trajphss{}'.format(i)]
        print()
        print("Start frame:", head[0])
#        print("Amplitudes:", mags)
#        print("Frequencies:", frqs)
#        print("Phases:", phss)

if __name__ == '__main__':
    main()
