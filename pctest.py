#!/usr/bin/env python3

import sys
import smspy
import smspy.stft
import smspy.peakdetection
import smspy.peakcontinuation
import scipy.io.wavfile
import numpy
import collections
import math


def detectPeaks(fs):
    def _(x):
        return numpy.array(list(smspy.peakdetection.peakDetection(x, fs)))
    return _


def main():
    fs, data = scipy.io.wavfile.read(sys.argv[1])
    dtype = data.dtype
    maxframes = int(sys.argv[2])
    detectPeaksV = detectPeaks(fs)
    print('Sampling Frequency:', fs)
    ff, stft = smspy.stft.stft(data, numpy.hamming, fs, 0.050, 0.020)
    N = int(0.050 * fs)
    peakSets = []
    for x in stft[0:maxframes]:
        peaks = [p[1] for p in detectPeaksV(x)]
        peakSets.append(collections.OrderedDict())
        for peak in peaks:
            peakc = x[peak]
            peakSets[-1][peak] = smspy.peakcontinuation.STFTDatum(
                numpy.linalg.norm(peakc),
                ff * peak / N,
                math.atan2(peakc.imag, peakc.real))
    peakSets = numpy.array(peakSets)
    trajs = smspy.peakcontinuation.peakContinuation(peakSets, [], 100, 1, 20,
                                                    100, 10, 10, 1)
    data = {}
    numTrajs = 0
    with open('output.dat', 'wb') as f:
        for index, traj in enumerate(trajs):
            data['trajhead{}'.format(index)] = numpy.array([traj.startframe])
            data['trajmags{}'.format(index)] = numpy.array(
                [d.magnitude for d in traj.magnitudes])
            data['trajfrqs{}'.format(index)] = numpy.array(
                [d.frequency for d in traj.magnitudes])
            data['trajphss{}'.format(index)] = numpy.array(
                [d.phase for d in traj.magnitudes])
            numTrajs += 1
    numpy.savez('output', **data)
    with open('output.dat', 'w') as f:
        f.write(repr([fs, numTrajs, dtype.name, 0.050, 0.020]))

if __name__ == '__main__':
    main()
