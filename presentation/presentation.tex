\environment env_presentation
\input{graphs.tex}
\useexternalfigure[algorithm_flowchart][../report/algorithm_flowchart.svg]
\startmode[outline]
  \def\StartSteps{}
  \def\StopSteps{}
  \def\FlushStep{}
  \setuplayout[top=0mm, header=8mm]
\stopmode
\starttext

\dontleavehmode
\blank[0.5in]
\title{Evaluation of Optimizations for Spectral Modeling Synthesis}

\startalignment[middle]
  David Li

  Mrs. DeCaro

  3 February 2012
\stopalignment
\setupheader[state=start]
\startslide[title={Introduction}]
  \StartSteps
    \startul[5,joinedup]
      \startitem All sounds are simply waves \FlushStep
      \stopitem
      \startitem Waves can be represented using sines and cosines \FlushStep
      \stopitem
      \startul[5]
        \startitem
          Complicated waves can be represented using the sum of sines and
          cosines \FlushStep
        \stopitem
        \startitem
          Example: square wave
        \stopitem
        \startGNUPLOTscript[square-wave]
          set samples 1000
          plot 2*((sin(x) > 0) - 0.5) with lines ls 1
        \stopGNUPLOTscript
        \useGNUPLOTgraphic[square-wave]
        \FlushStep
      \stopul
    \stopul
  \StopSteps
  \page
  \StartSteps
    \startul[5]
      \startitem
        We can approximate this square wave using a sum of sines \FlushStep
      \stopitem
      \startitem
        \startformula
          f(x) = \sin(t) + \frac13\sin(3t) + \frac15\sin(5t) + …
        \stopformula
        
        \startGNUPLOTscript[square-wave-sines]
          set samples 1000
          plot (4/pi)*(sin(x) + 0.333*sin(3*x) + 0.2*sin(5*x) + \
               0.1428*sin(7*x)) with lines ls 1
        \stopGNUPLOTscript
        \useGNUPLOTgraphic[square-wave-sines]
      \FlushStep
      \startitem
        Technically, if we added up an infinite number of such sines, we
        would have a perfect square wave (with some caveats) \FlushStep
      \stopitem
    \stopul
  \StopSteps
  \page[yes]
  \dontleavehmode
  \blank[0.5in]
  \onecite[squarewave]
  
  \onecite[understanding-dsp]
\stopslide


\startslide[title={Introduction || The Fourier Transform}]
  \StartSteps
    \startul[5,joinedup]
      \startitem The Fourier transform breaks down a sound into its
        frequencies \FlushStep \stopitem
      \startul
      \item Frequency = pitch you hear \FlushStep
      \item Magnitude = \quotation{volume} of the pitch \FlushStep
      \stopul
      \startitem Example: a 440|-|Hertz (A\low{4} note) sinusoid
      \stopitem
      \useGNUPLOTgraphic[fft-illustration-1] \FlushStep
    \stopul
  \StopSteps
  \StartSteps
    \page[yes]
    \startul[5]
      \startitem Take the Fourier transform of the sound:
      \stopitem
      \useGNUPLOTgraphic[fft-illustration-2] \FlushStep
      \startitem Graph of magnitude vs.~frequency || this graph shows all
        the frequencies, or pitches, contained in the sound \FlushStep
      \stopitem
      \startitem We call this the {\em discrete Fourier transform} (DFT) or
        {\em fast Fourier transform} (FFT) since it is performed on a
        computer \FlushStep
      \stopitem
    \stopul

    To summarize: the Fourier transform shows all the pitches contained in a
    sound and what volume those pitches are \FlushStep
  \StopSteps
  \page[yes]
  \dontleavehmode
  \blank[0.5in]
  \onecite[colorado-fourier-analysis]
  
  \onecite[understanding-dsp]
\stopslide

\startslide[title={Introduction || Spectral Modeling Synthesis}]
  
  \externalfigure[algorithm_flowchart][scale=1.25,wfactor=fit]
  \StartSteps
    \startul[5]
    \item Algorithm that uses FFT to analyze sound \FlushStep
    \item Variant used in experiment is the {\em sinusoidal
      analysis/synthesis system} \FlushStep
    \stopul
  \StopSteps
  \StartSteps
    \startul
    \item Uses {\em short|-|time Fourier transform} (STFT), variant of Fourier
      transform \FlushStep
      \startul
      \item Fourier transform analyzes entire signal at once \FlushStep
      \item Sounds change over time, need a way to account for this \FlushStep
      \item STFT solves this by taking FFT of short overlapping segments \FlushStep
        \startul
        \item Each segment = a frame \FlushStep
        \stopul
      \stopul
    \stopul
  \StopSteps
  \StartSteps
    \startul
    \item Designed for making sound transformation simple \FlushStep
      \startul
      \item Change pitch, speed, attack, etc.
      \stopul
    \item Four stages \FlushStep
      \startul
      \item STFT \FlushStep
      \item Peak detection || find the loudest pitches \FlushStep
      \item Peak interpolation || track the progression of a pitch \FlushStep
      \item Additive synthesis || add together pitches to recreate sound \FlushStep
      \stopul
    \stopul
  \StopSteps
  \StartSteps
    \startul
    \item Doesn't store exact sound || result will sound different \FlushStep
    \item FFT is inexact by nature \FlushStep
    \item Optimizations developed by Serra to mitigate this \FlushStep
    \item Three tested: \FlushStep
      \startul
      \item STFT zero|-|padding \FlushStep
      \item Peak interpolation during detection \FlushStep
      \item Peak continuation in reverse \FlushStep
      \stopul
    \stopul
  \StopSteps
  \page[yes]
  \dontleavehmode
  \blank[0.5in]
  \onecite[xserra-phd-thesis]
\stopslide
\startslide[title={Question \& Hypothesis}]
  \StartSteps
    \startul
      \item {\tfx Of all combinations of the three optimizations for the
        sinusoidal analysis/synthesis system, STFT zero-padding, peak
        continuation in reverse, and peak interpolation during detection
        (including enabling no optimizations), which will produce the most
        accurate synthesized sounds (lowest average root-mean-square error
        (RMSE) of the average 1024-point FFT bin magnitude) for a variety of
        input sounds?} \FlushStep
      \item {\tfx If sinusoidal synthesis is used to analyze and synthesize
        a variety of sounds using all possible combinations of the three
        optimizations, then the accuracy of the sound synthesized with all
        three optimizations will be highest because together, the
        zero-padding makes peak detection more accurate, the interpolation
        increases the accuracy of the detected peaks, and the reverse
        continuation helps to alleviate the \quotation{noisy} attack of many
        sounds.} \FlushStep
    \stopul
  \StopSteps
  \StartSteps
    \startul
    \item Impact \FlushStep
      \startul
      \item Eliminate useless/inefficient optimizations \FlushStep
      \item Improve applicability/areas of use for original algorithm
        \FlushStep
      \item Know more about how algorithm responds to different
        optimizations \FlushStep
      \stopul
    \stopul
  \StopSteps
\stopslide

\startslide[title={Experimental Procedure}]
  \StartSteps
    \startul[n]
    \item Implement algorithm \FlushStep
      \startul
      \item Language: Python 3.2 \FlushStep
      \stopul
    \item Run algorithm on dataset \FlushStep
    \item Aggregate results \FlushStep
    \stopul
    \startul
    \item Only one trial (determinism) \FlushStep
    \item Control group: no optimizations enabled \FlushStep
    \item Outside factors: \FlushStep
      \startul
      \item Algorithm may respond differently to different sounds \FlushStep
        \startul
        \item E.g. low|-|quality sound makes pitch detection harder \FlushStep
        \stopul
      \item Implementation errors \FlushStep
      \stopul
    \stopul
  \StopSteps
\stopslide

\startslide[title={Results}]
  \useGNUPLOTgraphic[data-graph]
\stopslide

\startslide[title={Discussion}]
  \StartSteps
    \startul
    \item Best performer: All three optimizations (31516 average) \FlushStep
    \item Best (single) performer: Peak interpolation (33226 average) \FlushStep
    \item Worst performer: Zero-padding with reverse peak continuation
      (114673 average) \FlushStep
    \item Control/no|-|optimizations: 96016 average \FlushStep
    \item Best input sound: flute (6000 to 7000 range) \FlushStep
    \item Worst input sound: 440-Hertz sine wave (20,000 to 300,000 range)
      \FlushStep
    \stopul
  \StopSteps

  \page[yes]

  \StartSteps\startul
  \item Use zero|-|padding with peak interpolation \FlushStep
  \item Using peak interpolation by itself works \FlushStep
  \item Some combinations of optimizations can't handle pure tones well
    \FlushStep
    \startul
    \item Zero|-|padding with reverse peak continuation had error of almost
      400,000 on sinusoid but only 7,000 on flute \FlushStep
    \stopul
  \item If processing power is an issue, drop zero|-|padding and just use
    other optimizations \FlushStep
  \stopul\StopSteps

\stopslide

\startslide[title={Conclusion}]
  
  \StartSteps\startul
  \item Impact of results \FlushStep
    \startul
    \item Save processing power by just using peak interpolation \FlushStep
    \item Algorithm seems to perform poorly under some conditions \FlushStep
    \stopul
  \stopul\StopSteps

  \page[yes]

  \StartSteps\startul
  \item Future research \FlushStep
    \startul
    \item Investigate full spectral modeling synthesis algorithm, not
      simplified sinusoidal analysis/synthesis system \FlushStep
    \item Test other optimizations \FlushStep
      \startul
      \item Equal|-|loudness curve \FlushStep
      \item Interpolation during synthesis \FlushStep
    \stopul
  \stopul\StopSteps

  \page[yes]

  \StartSteps\startul
  \item Possible errors \FlushStep
    \startul
    \item Incorrect implementation \FlushStep
    \stopul
  \stopul\StopSteps

\stopslide

\stoptext