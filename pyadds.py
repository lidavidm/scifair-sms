#!/usr/bin/env python3
import sys
import ast
import math
import numpy
import scipy.io.wavfile


def padFrames(frames, fNumber):
    for i in range(fNumber - len(frames)):
        frames.append([])


def magnitudeInterpolator(s, m_current, m_next):
    # s is length of synthesis frame (the stft hop size)
    # we interpolate from the current frame's magnitude to that of the next
    # frame!!
    mag = m_current
    for m in range(s):
        yield mag
        mag += (m_next - m_current) / s * (m + 1)


ONE_OVER_TWO_PI = 1 / (2 * math.pi)
TWO_PI = 2 * math.pi


def phaseInterpolator(s, frq_current, frq_next, phs_current, phs_next):
    THREE_OVER_S2 = 3 / (s ** 2)
    NTWO_OVER_S3 = -2 / (s ** 3)
    ONE_OVER_S = 1 / s
    ONE_OVER_S2 = 1 / (s ** 2)
    phase = phs_current
    for m in range(1, s + 1):  # last value of m is never used
        yield phase
        m = TWO_PI * round(
            ONE_OVER_TWO_PI * (
                (phs_current + (s * frq_current) - phs_next) +
                ((s / 2) * (frq_next - frq_current))))
        intermediate_1 = phs_next - phs_current - (frq_current * s) + m
        intermediate_2 = frq_next - frq_current
        eta = ((THREE_OVER_S2 * intermediate_1) -
               (ONE_OVER_S * intermediate_2))
        iota = ((NTWO_OVER_S3 * intermediate_1) +
                (ONE_OVER_S2 * intermediate_2))
        phase += m * (frq_current + (eta * m) + (iota * (m ** 2)))


def main():
    npz = numpy.load(sys.argv[1])
    header = ast.literal_eval(open(sys.argv[2]).read())
    print("Sampling Frequency:", header[0])
    fs = header[0]
    numTrajs = header[1]
    print("Number of trajectories:", numTrajs)
    dtype = numpy.dtype(header[2])
    dmax = numpy.iinfo(dtype).max
    print("Datatype:", dtype)
    hopsize = int(header[4] * fs)

    output = []
    curmax, curmin = 0, 0  # scaling to integers
    # we have to synthesize along a trajectory...

    for i in range(numTrajs):
        startframe = npz['trajhead{}'.format(i)][0]
        mags = npz['trajmags{}'.format(i)]
        frqs = npz['trajfrqs{}'.format(i)]
        phss = npz['trajphss{}'.format(i)]
        totalframes = startframe + len(mags)
        print()
        print("Trajectory {} of {}".format(i + 1, numTrajs))
        print("Start frame:", startframe)
        print("Trajectory length:", len(mags))
        if totalframes >= len(output):
            padFrames(output, totalframes)
        for index, frameNumber in enumerate(range(startframe, totalframes)):
            currentOutput = output[startframe + frameNumber]
            currentOutput.append([])
            currentOutput = currentOutput[-1]
            if index + 1 < len(mags):
                magnitudes = magnitudeInterpolator(
                    hopsize,
                    mags[index], mags[index + 1])
                phases = phaseInterpolator(
                    hopsize, frqs[index], frqs[index + 1],
                    phss[index], phss[index + 1])
            else:
                magnitudes = [mags[index]] * hopsize
                phases = [(m * frqs[index]) * phss[index]
                          for m in range(hopsize)]
            for magnitude, phase in zip(magnitudes, phases):
                currentOutput.append(magnitude * math.cos(phase))

    outsound = numpy.array(sum([
            [sum(x) for x in zip(*frame)] for frame in output
            ], []))

    outmax, outmin = abs(max(outsound)), abs(min(outsound))
    outpeak = outmax if outmax > outmin else outmin

    def scale(x):
        return dtype.type(round((x / outpeak) * dmax))
    scale = numpy.vectorize(scale)
    scipy.io.wavfile.write('synth.wav', header[0], scale(outsound))
    # here we zip output[frame] and sum to get s^l(m) after calculating each
    # trajectory - then we place those sums next to each other to get part
    # of a frame. Next, we scale to the datatype and write the output!

if __name__ == '__main__':
    main()
