\environment env_board

\input{data/gnuplot.tex}
\input{urls.tex}
\useexternalfigure[algorithm_flowchart][algorithm_flowchart.svg]
\setuppapersize[letter,portrait]
\starttext

\cardtitle{Background Information}\setupindenting[0.5in,yes]{\tfx

  All sounds are simply waves (or vibrations) and can be mathematically
  analyzed and manipulated as such. In particular, an important operation
  known as the {\em Fourier transform} can be used to break down a sound and
  find out what frequencies, or pitches, it contains and at what magnitude,
  or volume said frequencies are. As a demonstration, the graph depicts a
  440-Hertz sinusoidal signal (i.e., the signal is generated using $\sin(x)$
  and repeats 440 times per second):

  \useGNUPLOTgraphic[fft-illustration-1]

  And the next graph depicts the Fourier transform of the signal, showing
  the frequencies and their strengths (the magnitude vs.~frequency graph
  below is commonly referred to as the {\em magnitude spectrum}):

  \useGNUPLOTgraphic[fft-illustration-2]

  The Fourier transform is run on a computer as the discrete Fourier
  transform; the algorithm used is called the fast Fourier transform or
  simply the FFT. This difference stems from the fact that the computer can
  only process a finite amount of data, while in principle, the amount of
  data contained in nature is infinite. Consequently, sounds are represented
  using a finite number of {\em samples}, or measurements of the signal
  taken over time. (This terminology will come in useful later.)

  Spectral modeling synthesis (SMS), or more specifically, the sinusoidal
  analysis/synthesis system studied here, is an algorithm that leverages the
  Fourier transform in order to analyze and reconstruct the signal. The
  purpose is to provide a representation for the sound that can be easily
  modified || since the algorithm uses the FFT to break down the sound,
  individual pitches can be modified rather than imprecisely manipulating
  the sound as a whole. For instance, the frequency and speed of the sound
  can be changed (and in the latter case, without distorting the sound as
  much), two sounds can be mixed by applying the characteristics of one to
  another, and splice together different sounds. Xavier Serra developed the
  algorithm as well as three optimizations for it, which are the focus of
  this experiment: zero-padding, peak interpolation, and reverse peak
  continuation (described in the procedures). Each optimization tries to
  compensate for the inexact nature of the computer and the FFT (which can
  only discern pitches in small increments) but may or may not actually
  help, so this experiment measures the effect of each optimization on the
  result.

}

\framed[width=7.5in,align=flushleft]{

  \cardtitle{Procedures}
  \setupindenting[0.5in,yes]
  \externalfigure[algorithm_flowchart]

  Sinusoidal analysis/synthesis has four major stages: the short-time
  Fourier transform, peak detection, peak continuation, and additive
  synthesis. The first breaks down the input signals into individual
  pitches, showing the strength of each frequency present at each unit of
  time. Peak detection then looks for the strongest pitches, which are
  organized by peak continuation so that the algorithm can track the
  progression of a particular frequency over time. Finally, additive
  synthesis produces a sound by adding up all the pitches at each point in
  time. A high-level overview is provided here of the general idea behind
  the algorithm and its stages; for more details, please consult the report.

}

\framed[width=7.5in,align=flushleft]{
  \setupindenting[0.5in,yes]

  \subcardtitle{Short-time Fourier Transform (STFT)}

    Normally, the Fourier transform finds all the frequencies contained in
    the entire length of input signal. However, most sounds will change over
    time, so sinusoidal synthesis uses the short-time Fourier transform
    (STFT) instead, which takes the Fourier transform of short overlapping
    segments (frames) of the input signal. This way, we get magnitude
    (i.e. volume) vs.~frequency (pitch) vs.~time data. Each unit of time is
    termed a {\em frame} and corresponds to several milliseconds of actual
    time.

    If the zero-padding optimization is enabled, then each such segment is
    \quotation{stuffed} or \quotation{padded} by adding samples of zeros
    until the segment's length is a power of two. This increases the
    resolution, or smallest discernible change in frequency, of the Fourier
    transform, which increases as the amount of data increases.

}

\framed[width=7.5in,align=flushleft]{\setupindenting[0.5in,yes]

  \subcardtitle{Peak Detection}

  \useGNUPLOTgraphic[peak-detection-illustration]

  The STFT identifies all the frequencies contained in the input, but not
  all are strong enough to matter || some are too weak to be heard. Peak
  detection solves this by looking for those pitches whose magnitudes are
  local maximums (on a graph of magnitude vs.~frequency, the algorithm finds
  the peaks of the curves), which are termed {\em peaks}. Additionally, the
  algorithm only searches frequencies in the range of human hearing, so it
  discards data that would not matter to a listener.

  If the peak interpolation optimization is enabled, then the peaks are
  adjusted using parabolic interpolation: the peak, as well as the two
  minimums or \quotation{valleys} surrounding it, are taken, and a parabola
  is constructed that goes through these points. Then, the vertex of that
  parabola is taken to be the actual peak.

}

\framed[width=7.5in,align=flushleft]{\setupindenting[0.5in,yes]

  \subcardtitle{Peak Continuation}

  \useGNUPLOTgraphic[peak-continuation-illustration]

  The frequency information is organized by frame/time, but it is more
  convenient for the algorithm to additionally sort the peaks by frequency,
  which is what peak continuation does. For example, if frames 5, 6, and 7
  contain peaks of frequency 442, 431, and 450 Hertz, then together they
  would form a {\em trajectory} that tracks the progression of a pitch of
  approximately 440 Hertz over time (an A\low{4} note). As an example, the
  graph depicts a set of peaks, with lines through the peaks representing
  the trajectories.

  If the reverse continuation optimization is enabled, then the frames are
  reversed with respect to time before creating trajectories, and each
  trajectory is again reversed afterward. Therefore, the process starts at
  the end of the signal and travels to its beginning.

}

\framed[width=7.5in,align=flushleft]{\setupindenting[0.5in,yes]

  \subcardtitle{Additive Synthesis}

  Just as the Fourier transform can decompose a complicated signal (one with
  multiple frequencies) into the sum of many signals, each of only one
  pitch, a complicated signal can be constructed by adding together simpler
  ones, which is what additive synthesis does. For each frame, several
  samples are output since each frame corresponds to several milliseconds,
  and furthermore, the magnitude and frequency within each trajectory is
  interpolated between frames. As an example, if a trajectory contained two
  peaks of frequency 440 and 450 Hertz, and 10 samples were to be output,
  the first would be of frequency 440 Hertz, the second 441, the third 442,
  and so on. This way, the pitches do not suddenly jump around but rather
  smoothly change over time.

  \blank[0.25in]
  \subcardtitle{Data Analysis}

  Both the original and synthesized signal are split into segments of length
  1024 samples, and then the Fourier transform is taken of each segment. All
  the magnitude spectra of each signal are averaged together, then the
  difference between the final and original spectra is computed. Finally,
  the square root of the sum of the squares of the differences (the RMSE, or
  root-mean-square error) is calculated, which is what is depicted in the data
  table.

}

\framed[width=7.5in,align=flushleft]{\setupindenting[0.5in,yes]

  \subcardtitle{Applications}

  Spectral modeling synthesis/sinusoidal synthesis was originally conceived
  as a way to represent sounds so that their speed, pitch, and other
  characteristics could be easily changed. This experiment helps improve on
  the original algorithm by determining how effective various optimizations
  are on the quality of the output, showing that one, zero padding, is not
  worth the computational expense since it either provides only a small
  benefit or dramatically worsens the quality of the result. For companies
  like Yamaha and Bose that manufacture audio hardware, being able to use
  the most efficiently implemented algorithms can reduce costs. Although
  execution time was not measured in this experiment, simply knowing that
  (for example) zero-padding only helps when used in conjunction with peak
  interpolation or that alone, interpolation is the most effective can help
  people making implementation and usage decisions. Of course, the algorithm
  studied is already useful for modifying the pitch and speed of sounds; for
  instance, one can slow down a signal simply by outputting more signals for
  each frame or change the pitch by modifying the peaks in a particular
  trajectory before synthesis. Furthermore, because the algorithm discards
  data based on whether the human auditory system is likely to be able to
  actually register the information, it could potentially serve as a way to
  store and transmit audio data, such as for cellphones or VoIP
  communications systems, and in such cases, saving computational power by
  not using parts of the algorithm that are not efficient can be
  valuable. Clearly, this experiment has a wide range of applications due to
  the need for efficiency and the wide variety of uses for the original
  algorithm.

}

\stoptext