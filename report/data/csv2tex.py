#!/usr/bin/env python3
import csv
import sys

reader = csv.reader(open(sys.argv[1]), delimiter=',', quotechar='"')
print(r"\bTABLE[split=yes]")
header = next(reader)

print(r"\bTABLEhead")
print(r"\bTR")
for cell in header:
	print(r"\bTH", cell, r"\eTH")
print(r"\eTR")
print(r"\eTABLEhead")
print()

print(r"\bTABLEbody")
for row in reader:
	print(r"\bTR")
	if row[0]:
		print(r"\bTD[nr=7]", row[0], r"\eTD")
	else:
		pass#print(r"\bTD", row[0], r"\eTD")
	for cell in row[1:]:
		if cell:
			print(r"\bTD", cell, r"\eTD")
	print("\eTR")
	print()

print(r"\eTABLEbody")
print(r"\eTABLE")
