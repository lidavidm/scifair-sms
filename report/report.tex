\environment env_scifair

\input{data/gnuplot.tex}
\input{urls.tex}

\useexternalfigure[setup][setup.png]
\useexternalfigure[algorithm_flowchart][algorithm_flowchart.svg]


\starttext

\title{Evaluation of Optimizations for Spectral Modeling Synthesis}

\blank[0.5in]

\startalignment[middle]

  David Li

  Mrs.~DeCaro

  3 February 2012

  \blank[0.5in]

  \url[bitbucket-source]

\stopalignment

\blank[0.5in]

\startsection[title={Abstract}]

  \startabstract
    
    Spectral modeling synthesis is a method for analyzing and synthesizing
    sound intended to provide a format for transforming the sound. This
    experiment evaluates the effect of three optimizations, short-time
    Fourier transform zero-padding, peak continuation in reverse, and peak
    interpolation during detection, on the quality of the resulting audio
    for a variant of this algorithm, sinusoidal synthesis. Six input sounds,
    which included both recordings of instruments and synthesized sinusoids,
    were analyzed and re-synthesized for each possible combination of
    enabled optimizations. Quality, measured by the root-mean-square error
    of the average of the 1024 point FFT bin magnitude, was highest
    (i.e. the error was lowest) when all three were enabled and worst when
    only zero-padding was used. Therefore, all are deemed useful if used
    together, although different usage scenarios may necessitate different
    combinations of optimizations.

  \stopabstract

\stopsection

\page[yes]

\startsection[title={Purpose \& Hypothesis}]

  This experiment aims to evaluate the effectiveness of three optimizations,
  STFT zero-padding, peak continuation in reverse, and peak interpolation
  during detection, for sinusoidal synthesis on the quality of the resulting
  audio. Quality is measured as the average error of the magnitude of the
  FFT bins as compared to the original.

  Question: Of all combinations of the three optimizations for the
  sinusoidal analysis/synthesis system, STFT zero-padding, peak continuation
  in reverse, and peak interpolation during detection (including enabling no
  optimizations), which will produce the most accurate synthesized sounds
  (lowest average root-mean-square error (RMSE) of the average 1024-point
  FFT bin magnitude) for a variety of input sounds?

  Hypothesis: If sinusoidal synthesis is used to analyze and synthesize a
  variety of sounds using all possible combinations of the three
  optimizations, STFT zero-padding, peak continuation in reverse, and peak
  interpolation during detection, then the accuracy of the sound synthesized
  with all three optimizations will be highest (average error will be
  lowest) because together, the zero-padding makes peak detection more
  accurate, the interpolation increases the accuracy of the detected peaks,
  and the reverse continuation helps to alleviate the \quotation{noisy}
  attack of many sounds \cite[xserra-phd-thesis].

\stopsection

\startsection[title={Background Information}]

  All sounds are simply waves (or vibrations) and can be mathematically
  analyzed and manipulated as such. In particular, an important operation
  known as the {\em Fourier transform} (from a branch of mathematics called
  Fourier analysis) can be used to break down a sound and find out what
  frequencies, or pitches, it contains and at what magnitude, or volume
  \cite[colorado-fourier-analysis]. As a demonstration, the graph depicts a
  440-Hertz sinusoidal signal (i.e., the signal is generated using $\sin(x)$
  and repeats 440 times per second):

  \useGNUPLOTgraphic[fft-illustration-1]

  And the next graph depicts the result of the Fourier transform of the
  signal, showing the frequencies and their strengths present (the magnitude
  vs.~frequency graph below is commonly referred to as the {\em magnitude
    spectrum}, plural spectra):

  \useGNUPLOTgraphic[fft-illustration-2]

  All sounds can be represented as the sum of many simple\footnote{i.e.,
    sinusoids of a particular frequency, magnitude, and phase.} pitches, or
  harmonics; the Fourier transform is useful since it allows us to determine
  which frequencies are contained in a particular signal
  \cite[colorado-fourier-analysis].  The Fourier transform, when run on a
  computer, is known as the discrete Fourier transform (DFT); the algorithm
  used to actually perform the calculations is called the fast Fourier
  transform or simply the FFT. This difference stems from the fact that the
  computer can only process a finite amount of data, while in principle, the
  amount of data contained in nature is infinite. Consequently, sounds are
  represented using a finite number of {\em samples} (referred to as
  input/output frames in later parts of this report), or measurements of the
  signal taken over time, even though mathematically they are quite
  different from this representation\footnote{For instance, the Fourier
    transform assumes the signal is periodic.}. (This terminology will come
  in useful later.) Furthermore, the analysis performed by the DFT/FFT is
  inexact, so algorithms that depend on it often must compensate for this.

  The spectral modeling synthesis (SMS) algorithm (or more specifically, a
  simpler variant termed the {\em sinusoidal analysis/synthesis
    system}\footnote{Sinusoidal synthesis was chosen due to time
    constraints; the differences from SMS consist of omitted stochastic
    analysis and synthesis stages.}, referred to as {\em sinusoidal
    synthesis} here) leverages a variant of the Fourier transform, the {\em
    discrete short-time Fourier transform} (STFT), which can analyze a sound
  whose frequencies change over time by breaking the sound into short
  segments called frames and analyzing each individually
  \cite[xserra-phd-thesis], rather than processing the entire signal at once
  as the normal DFT does. The sinusoidal synthesis algorithm begins by
  taking the STFT of the input signal, then analyzes the resulting
  frequencies by first finding the {\em peaks} in the magnitude spectrum of
  each frame, which are essentially the \quotation{loudest} pitches. Then,
  peaks with a similar frequency are ordered into a {\em trajectory}, which
  essentially tracks the progression of a particular pitch || a particular
  harmonic || throughout the sound. Using this information, the algorithm
  recreates the sound in {\em additive synthesis} by summing sine waves of a
  particular frequency and magnitude to create the output. A more detailed
  explanation can be found in the \in{procedures.}{}[section:procedures]

  As this algorithm does not store the exact sound, but rather attempts to
  reproduce it later given the trajectories, the result obviously will sound
  different from the input. However, several optimizations were developed in
  order to mitigate this. The three evaluated here are STFT zero-padding,
  peak interpolation during detection, and peak continuation in
  reverse. STFT zero-padding refers to appending zeros to the input signal
  to increase the resolution (how many frequencies/pitches can be
  discerned). In this case, since the STFT is implemented by taking the FFT
  of small, overlapping segments of the original input, zero-padding would
  add zeros to each FFT. Peak interpolation simply uses equations derived by
  Serra to estimate the true location of a detected peak with a parabolic
  interpolation. This produces a peak with a more accurate magnitude and
  frequency. Peak continuation in reverse tries to optimize for instrumental
  sounds by looking for pitches starting at the end of the sound and
  progressing to the start. This is due to the fact that many sounds have a
  \quotation{noisy} attack (or beginning) but then settle down
  \cite[xserra-phd-thesis]. Therefore, by trying to build the peak
  trajectories in reverse, the algorithm is more likely to ignore the noise
  and consequently produce a more accurate sound.

  Originally, spectral modeling synthesis was conceived as a representation
  for sounds that would make it simpler to modify characteristics of a
  sound, such as the pitch, attack, and speed, compared to trying to
  directly modify the sound samples. Determining the effectiveness of the
  various optimizations for the algorithm matters since the accuracy of the
  analysis and the synthesized sound matters when trying to use SMS for such
  purposes. Xavier Serra, the developer of the algorithm, did not indicate
  the effectiveness of some of the optimizations described in the original
  report, so this experiment aims to evaluate them.

\stopsection

\startsection[title={Materials}]

  This experiment runs on a computer and therefore requires no materials per
  se. For reference, the equipment used for the data in this report is
  listed, but it should not make a difference.

  \startitemize[n, joinedup]
  \item Computer: Acer Aspire 7552G-6061
    \startitemize[n, joinedup]
    \item Processor: AMD Phenom II X4 N950 (2.1 GHz)
    \item Memory: 4GB
    \item Hard Drive Space: 500GB (estimated need: 700 MB, 600 MB of which
      is temporary; additional space required for dependencies)
    \stopitemize
  \item Operating System: Ubuntu Linux 11.10 64-bit edition
  \item Language: Python 3.2
  \stopitemize

  More information about dependencies and usage can be found in the README
  file of the source code at \hyperfoot[bitbucket-scifair-sms].
  
\stopsection

\reference[section:procedures]{}
\startsection[title={Procedures}]

  \figurecap[algorithm_flowchart]{A high-level overview of the algorithm.}

  Sinusoidal analysis/synthesis has four major stages: the short-time
  Fourier transform, peak detection, peak continuation, and additive
  synthesis. The first breaks down the input signal into individual pitches,
  showing the strength of each frequency present at each frame, or unit of
  time. Peak detection then looks for the strongest pitches, which are
  organized by peak continuation so that the algorithm can track the
  progression of a particular frequency over time. Finally, additive
  synthesis produces a sound by adding up all the pitches in each frame.

  As this experiment consists of running a computer program, there are no
  procedures per se; instead, a description of the algorithms used
  follows. Each section contains both a general explanation as well as
  additional explanations and definitions. For the most detail, the source
  code can be obtained from \hyperfoot[bitbucket-source].

  \startsubsection[title={Short-time Fourier Transform}]

    Normally, the Fourier transform finds all the frequencies contained in
    the entire length of input signal. However, most sounds will change over
    time, so sinusoidal synthesis uses the short-time Fourier transform
    (STFT) instead, which takes the Fourier transform of short overlapping
    segments (frames) of the input signal. This way, we get magnitude
    (i.e. volume) vs.~frequency (pitch) vs.~time data. Each segment
    corresponds to a unit of time within the algorithm and is termed a {\em
      frame} that corresponds to several milliseconds of actual time.

    If the zero-padding optimization is enabled, then each such segment is
    \quotation{stuffed} or \quotation{padded} by adding samples of zeros
    until the segment's length is a power of two before taking the Fourier
    transform. This increases the resolution, or smallest discernible change
    in frequency, of the Fourier transform, which increases as the length of
    data increases.

    The particular implementation used in this experiment is based on one
    from a StackOverflow answer (\hyperfoot[so-python-stft]) by
    \hyperfoot[so-python-stft-answerer], used under the terms of the
    \hyperfoot[cc-by-sa-2.5] and can be found in the file {\tt
      smspy/stft.py}.

    The STFT gives a list of magnitudes which are complex-valued. To get the
    linear magnitude, we take the magnitude of each complex number ($|a +
    bi| = \sqrt{a^2 + b^2}$), and to get the decibel magnitude, we calculate
    $20\log_{10}|x|$ where $x$ is a value in the linear magnitude
    spectrum.
    
  \stopstage

  \startstage[title={Peak Detection}]

    \useGNUPLOTgraphic[peak-detection-illustration]

    The STFT identifies all the frequencies contained in the input, but not
    all are strong enough to matter || some are too weak to be heard. Peak
    detection solves this by looking for those pitches whose magnitudes are
    local maximums (on a graph of magnitude vs.~frequency, the algorithm
    finds the peaks of the curves), which are termed {\em peaks}.
    Additionally, the algorithm only searches frequencies in the range of
    human hearing, so it discards data that would not matter to a listener.

    Let $k_\beta$ be a bin number\footnote{The bin number is an index
      relating a frequency to a magnitude; for instance, bin number 11 would
      relate the 11\high{th} detected frequency to its magnitude in the
      magnitude spectrum.}, $X$ be the linear magnitude spectrum, and
    $\hat{X}$ be the decibel magnitude spectrum.

    Let $k_\beta$ be a peak when $\hat{X}(k_\beta - 1) \leq \hat{X}(k_\beta)
    \geq \hat{X}(k_\beta + 1)$\footnote{This differs from Serra's
      definition, $|\hat{X}(k_\beta - 1)| \leq |\hat{X}(k_\beta)| \geq
      |\hat{X}(k_\beta + 1)|$, because in the text, it is defined as a local
      maximum, not a local extremum.}. For this peak, let $k_\gamma -$ and
    $k_\gamma +$ be the bin numbers nearest local minimums of the magnitude
    spectrum on the left and right, respectively.

    Let the peak height $h(k_\beta)$ be defined as

    \placeformula[formula:peak-height]
    \startformula
      h(k_\beta) = \hat{X}(k_\beta) - \frac{[\hat{X}(k_\gamma -) +
          \hat{X}(k_\gamma +)]}{2}
    \stopformula

    The exact algorithm for finding peaks can be found in the source code,
    so we will not describe it fully here, but the procedure simply searches
    for peaks within a certain frequency range and a certain magnitude
    range, then discards peaks below a particular height (as defined in
    \in{formula}{}[formula:peak-height]).

    \startsubsubsection[title={Peak Interpolation}]

      \useGNUPLOTgraphic[peak-interpolation-illustration]
        
      Peak interpolation tries to compensate for the inaccuracies inherent
      in the STFT/FFT by using the detected peak, as well as the two
      minimums (valleys) on either side of it, to extrapolate the true
      position of the peak by constructing a parabola through the three
      points as depicted.

      Specifically, peak interpolation tries to find a parabola of the form
      $y(x) = a (x-p)^2 + b$, where $p$ is the location of the interpolated
      peak. If $k_\beta$ is the location (bin number) of the detected peak,
      $X$ is the input signal, $\alpha$ is $20 \log_{10}|X_{k_\beta - 1}|$,
      $\alpha$ is $20 \log_{10}|X_{k_\beta}|$, and $\gamma$ is $20
      \log_{10}|X_{k_\beta + 1}|$, then by setting $y(-1) = \alpha$, $y(0) =
      \beta$, and $y(1) = \gamma$, we find that

      \startformula
        p = \frac12 \frac{\alpha - \gamma}{\alpha - 2\beta + \gamma}
      \stopformula

      \cite[xserra-phd-thesis]. Therefore, the estimated peak bin location is

      \startformula
        k^\ast = k_\beta + p
      \stopformula

      From this, the frequency of the peak in Hertz is $f_sk^\ast + p$ and the
      magnitude $y(p)$ is $\beta - \frac14 (\alpha - \gamma) p$. The
      magnitude must be multiplied by a scale factor, 

      \startformula
        \frac2{\sum^{M-1}_{m=0} w(m)}
      \stopformula

      where $w(m)$ is the value of the window transform at time $m$ and $M$
      is the length of the transform. The implementation then follows from
      these definitions.

    \stopsubsubsection

  \stopstage

  \startstage[title={Peak Continuation}]

    \usegraph[peak-continuation-illustration]{The peak continuation
      process. (Point size = magnitude)}

    The frequency information is organized by frame/time, but it would be
    more convenient for the algorithm to additionally sort the peaks by
    frequency. For example, if frames 5, 6, and 7 contain peaks of frequency
    442, 431, and 450 Hertz, then together they would form a {\em
      trajectory} that tracks the progression of a pitch of approximately
    440 Hertz over time (an A\low{4} note). As an example, the graph depicts
    a set of peaks, with lines through the peaks representing the
    trajectories.

    Peak continuation takes a list of lists of peaks, where each sublist
    represents a frame of audio, and organizes them into trajectories, which
    contain a list of peaks, with one peak per frame, as well as the frame
    at which it started. We use an intermediate data structure called a {\em
      guide} to create these trajectories. Each guide contains a list of
    peaks, the starting frame number, and the frequency it is currently
    tracking. If the reverse peak continuation optimization is enabled, then
    we simply reverse the outer list before running the algorithm, then
    reverse each trajectory afterward.

    Let $f_1, f_2, ..., f_p$ be the guide frequencies at frame $n-1$ and let
    $g_1, g_2, ..., g_p$ be the new guide frequencies to be calculated for
    frame $n$. Each guide $i$ attempts to claim the peak closest in frequency,
    i.e. the peak with frequency $f$ for which $|f - f_i|$ is a minimum. The
    parameter {\tt max-peak-deviation} represents the largest allowed change
    in frequency. The three possible situations for this process are as
    follows:

    \startitemize[n, joinedup]
    \item If a suitable peak is found, allow the peak to continue.
    \item If no suitable peak is found, \quotation{kill} the guide and
      update its frequency to 0. Do not track the guide anymore and turn it
      into a trajectory.
    \item If a suitable peak is found, but it has already been claimed, give
      it to whichever guide is closer. Then, the \quotation{loser} must
      search for another peak, following these three steps.
    \stopitemize

    To update a guide, add the product of the {\tt peak contribution} and
    the difference of the claimed peak's frequency and the tracked
    frequency\footnote{This is a step taken from what Serra terms the
      \quotation{deterministic plus residual model.}}. Each unused peak is
    used to initialize a new guide by starting the guide at frame $n-1$ with
    zero frequency and updating it to the frequency of the unused peak. Each
    \quotation{dead} guide becomes a trajectory by discarding the tracked
    frequency. After going through all frames, convert the remaining guides
    to trajectories and return all trajectories made.

  \stopstage

  \startstage[title={Additive Synthesis}]

    Because the original sound was analyzed in short segments, the resulting
    sound will be synthesized in short segments termed \quotation{synthesis
      frames} (to distinguish them from the frames of the input/output
    sound; the number of synthesis frames will be less than the number of
    input/output frames), with each synthesis frame containing multiple
    output samples.

    Let $s^l(m)$, the value of output sample $m$ of synthesis frame $l$, be

    \startformula
      \sum^{R_l}_{r=1} A_r(m) \cos(m\Theta_r(m)),\;\;\;\; m = 0, 1, ..., S-1
    \stopformula

    where $R$ is the number of trajectories present at frame $n$, $A_r(m)$
    is the magnitude for this frame at output sample $m$, $\Theta_r(m)$ is
    the phase, and $S=\:${\tt hopsamp} is the length of the synthesis
    frame. The amplitude and phase are functions that interpolate between
    the values of the previous and current peak, defined by

    \startformula \startalign
      \NC A_r(m) \NC = A^{l-1}_r + \frac{A^l_r - A^{l-1}_r}{S} m \NR
      \NC \theta_r(m) \NC = \omega^{l-1}_r + \frac{\omega^l_r - \omega^{l-1}_r}{S}m
    \stopalign \stopformula

    where $A^l_r$ is the amplitude of the peak in the current trajectory at
    synthesis frame $l$ and $\omega^l_r$ is the frequency\footnote{The
      frequency is interpolated linearly, as in \quotation{magnitude-only
        analysis/synthesis}.}.

    After generating all the synthesis frames, they are simply concatenated
    in order to produce the final output sound.

  \stopstage

  \startstage[title={Data Analysis}]

    Both the original and synthesized signal are split into segments of
    length 1024 samples, and then the Fourier transform is taken of each
    segment. All the magnitude spectra of each signal are averaged together,
    then the difference between the final and original spectra is
    computed. Finally, the square root of the sum of the squares of the
    differences (the RMSE, or root-mean-square error) is calculated.

  \stopstage

  \startsubsubsection[title={Safety Considerations}]

    This project does not cause any safety issues.

  \stopsubsubsection
\stopsection

\startsection[title=Data]

  \input{data/rmse.tex}
  \usegraph[data-graph]{Graph of the data.}

\stopsection

\startsection[title={Data Analysis}]

  In terms of the concepts behind Fourier analysis, the RMSE of the FFT bin
  magnitude indicates how close the synthesized and original sound are in
  terms of \quotation{how much} of a particular frequency they contain. A
  high error implies that the \quotation{strengths} of pitches differed
  greatly between the original and output, and conversely, a low error
  indicates that the magnitudes were close.

  As predicted, sinusoidal synthesis performed best with all three
  optimizations enabled, with an average RMSE of 31,516. Enabling
  zero-padding and peak interpolation resulted in an average RMSE of 31,595,
  indicating that (for this dataset) reverse peak continuation had only a
  minor effect on quality. Surprisingly, having no optimizations was not the
  worst combination; instead, enabling STFT zero-padding and reverse peak
  continuation resulted in the worst accuracy (an average root-mean-square
  error of 114,673), which was slightly worse than simply enabling
  zero-padding alone (114,521). The algorithm performed quite variably with
  regards to the input file as well; while the flute sound had an RMSE of
  around 6,000 for all combinations of optimizations, that of the generated
  600-Hertz sine wave ranged from 23,000 to 377,000. Clearly, while
  enabling these optimizations may produce a better output, the difference
  is not great, and some options produce much worse sound when used
  alone. Zero-padding was the worst optimization to enable, while peak
  interpolation was the best, as it was three times more accurate than
  having no optimizations and was quite close, with an error of about
  33,000, to having all optimizations enabled. Since computation of the FFT
  can be quite expensive, especially with more input data (which is what
  zero-padding does), only using peak interpolation rather than all three
  optimizations may be desirable for faster execution.

\stopsection

\startsection[title=Conclusion]

  This experiment aimed to evaluate the effectiveness of three
  optimizations, STFT zero-padding, peak continuation in reverse, and peak
  interpolation during detection, for sinusoidal synthesis on the quality of
  the resulting audio. The hypothesis was shown to be correct because, as
  predicted, the error was lowest (31,516) when all three optimizations were
  enabled. However, in terms of only enabling one optimization, peak
  interpolation performed the best with an error of 33,226, while
  zero-padding was the worst with an error of 114,521. This data could be
  used when computational expense is an issue: since computing the Fourier
  transform can take some time, using peak interpolation and not using
  zero-padding will produce an output of comparable quality with fewer
  computations. Future research could focus on the effects of these
  optimizations on actual spectral modeling synthesis (which differs mainly
  in a more complex peak continuation stage and a separate stochastic
  synthesis stage) or on the effects of other, unimplemented optimizations
  such as an equal-loudness curve.

  Spectral modeling synthesis/sinusoidal synthesis was originally conceived
  as a way to represent sounds so that their speed, pitch, and other
  characteristics could be easily changed. This experiment helps improve on
  the original algorithm by determining how effective various optimizations
  are on the quality of the output, showing that one, zero padding, is not
  worth the computational expense since it either provides only a small
  benefit or dramatically worsens the quality of the result. For companies
  like Yamaha that manufacture audio hardware, being able to use the most
  efficiently implemented algorithms can reduce costs. Although execution
  time was not measured in this experiment, simply knowing that (for
  example) zero-padding only helps when used in conjunction with peak
  interpolation or that alone, interpolation is the most effective can help
  people making implementation and usage decisions. Furthermore, because the
  algorithm discards data based on whether the human auditory system is
  likely to be able to actually register the information, it could
  potentially serve as a way to store and transmit audio data, such as for
  cellphones or VoIP communications systems, and in such cases, saving
  computational power by not using parts of the algorithm that are not
  efficient can be valuable. Clearly, this experiment has a wide range of
  applications due to the need for efficiency and the wide variety of uses
  for the original algorithm.

  Errors that may have affected this experiment include implementation bugs
  and a less-than-comprehensive data set. Although this experiment was
  implemented directly from the descriptions in Serra's PhD thesis,
  differences in understanding or interpretation may have affected the
  results. Furthermore, the final code used included algorithms from other
  sections of the thesis as well as other sources (in particular, the STFT
  code). More importantly, the final results often did not sound even
  remotely close to the original; however, it is not known whether the cause
  is due to an implementation error, poor parameter choices, the nature of
  sinusoidal synthesis itself, or other reasons. The implementation used
  magnitude-only analysis\footnote{This was due to time constraints.} (it
  ignored phase data), which tends to produce a less accurate sound (though
  in many cases the perceived quality is the same) and therefore may have
  affected the results as well \cite[xserra-phd-thesis]. Of course, bugs
  could be present; no implementation of sinusoidal synthesis could be found
  to check the results of this experiment. Many of these trade-offs were due
  to a lack of time; future experiments could avoid this by building on this
  codebase rather than creating one from scratch (as the only available
  previous implementations seem to be for spectral modeling
  synthesis). Finally, the results may not be as comprehensive as possible
  since not all types of instruments were represented in the input dataset,
  including percussion and brass instruments, and the algorithm tended to
  perform quite differently for actual and synthesized sounds. For example,
  the synthesized 600 Hertz sinusoid was consistently poor across all
  combinations of optimizations. Future experiments would simply need to
  find more input samples to test against to avoid this possible bias.
  
\stopsection

\startsection[title=Bibliography]
  \placepublications[criterium=all]
\stopsection

\reference[appendix:attribution]{}
\startappendix[title={Attribution}]

  This project makes use of three sounds, each used under the terms of their
  respective licenses:

  \startitemize[n,joinedup]
  \item {\tt experiment/65508__juskiddink__flute1.wav} and {\tt
    experiment/inputs/65508__juskiddink__flute1_cropped.wav}: Used under the
    \hyperfoot[cc-by-3.0], from \hyperfoot[freesound-flute].

  \item {\tt experiment/inputs/68448__pinkyfinger__piano-g_mono.wav}, {\tt
    experiment/68448__pinkyfinger__piano-g.wav}: Used under the
    \hyperfoot[cc-0], from \hyperfoot[freesound-piano].

  \item {\tt experiment/inputs/violin_c.wav}: Used under the
    \hyperfoot[cc-0], from \hyperfoot[freesound-violin].

  \stopitemize
  
\stopappendix

% \reference[appendix:1]{}
% \startappendix[title={Parameter Values}]

% \bTABLE[split=repeat]
% \bTABLEhead
% \bTR
% \bTH Procedure\eTH
% \bTH Parameter Name\eTH
% \bTH Default Value\eTH
% \bTH Description\eTH
% \eTR
% \eTABLEhead
% \bTABLEbody

% \bTR
% \bTD[nr=4] STFT\eTD \bTD window\eTD
% \bTD {\tt numpy.hamming}\eTD \bTD The window function to use.\eTD\eTR

% \bTR
% \bTD framesize\eTD
% \bTD {\tt 0.050} (50 ms)\eTD
% \bTD The size of the analysis frame in milliseconds.\eTD
% \eTR

% \bTR
% \bTD hop\eTD
% \bTD {\tt 0.020} (20 ms)\eTD
% \bTD The size of the hop size in milliseconds.\eTD
% \eTR

% \bTR
% \bTD padding\eTD
% \bTD {\tt False}\eTD
% \bTD A flag indicating whether to enable the zero-padding optimization.\eTD
% \eTR



% \bTR
% \bTD[nr=3] Peak Detection\eTD \bTD interpolate\eTD
% \bTD {\tt False}\eTD \bTD A flag indicating whether to enable the peak
% interpolation optimization.\eTD\eTR

% \bTR
% \bTD minimum peak height\eTD
% \bTD {\tt 40}\eTD
% \bTD The minimum height for a peak to be detected.\eTD
% \eTR

% \bTR
% \bTD frequency range\eTD
% \bTD {\tt 40} Hz to {\tt 16} kHz\eTD
% \bTD The frequency range in which peaks are searched for. A frequency is
% converted into a bin number with the formula $\frac{fN}{f_s}$ where $f$ is
% the frequency, $N$ is the FFT size ({\tt hopsamp}) and $f_s$ is the sampling
% rate of the input audio. \eTD
% \eTR


% \bTR
% \bTD[nr=3] Peak Continuation\eTD \bTD detect_in_reverse\eTD
% \bTD {\tt False}\eTD \bTD A flag indicating whether to enable the reverse
% peak detection optimization.\eTD\eTR

% \bTR
% \bTD peak contribution\eTD
% \bTD {\tt 1}\eTD
% \bTD The weight used to update the guide tracking frequency.\eTD
% \eTR

% \bTR
% \bTD maximum peak deviation\eTD
% \bTD {\tt 1000}\eTD
% \bTD The maximum change in frequency of a guide between frames.\eTD
% \eTR


% \eTABLEbody
% \eTABLE

% \stopappendix

\stoptext

%  LocalWords:  STFT
