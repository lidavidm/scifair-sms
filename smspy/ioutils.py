import numpy

from .peakdetection import Peak

def savePeaks(peaks, output):
    """
    Save a list of peaks to a NumPy file.

    Format:

    peaks = [Frame]
    Frame = [Peak]
    """
    magnitudes = []
    frequencies = []
    phases = []
    for frame in peaks:
        mags, freqs, phss = [], [], []
        for peak in frame:
            mags.append(peak[0])
            freqs.append(peak[1])
            phss.append(peak[2])
        magnitudes.append(numpy.array(mags))
        frequencies.append(numpy.array(freqs))
        phases.append(numpy.array(phss))
    magnitudes = numpy.array(magnitudes)
    frequencies = numpy.array(frequencies)
    phases = numpy.array(phases)
    numpy.savez(output, magnitudes=magnitudes, frequencies=frequencies,
                phases=phases)


def loadPeaks(peakfile):
    data = numpy.load(peakfile)
    mags = data['magnitudes']
    frqs = data['frequencies']
    phss = data['phases']
    frames = []
    for f in zip(mags, frqs, phss):
        frames.append([Peak(m, f, p) for (m, f, p) in zip(*f)])
    return frames