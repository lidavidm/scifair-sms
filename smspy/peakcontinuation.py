import collections
from .peakdetection import Peak

class Trajectory(collections.namedtuple('Trajectory', 'startframe peaks')):
    def __repr__(self):
        return "<Trajectory startframe: {} peaks: {}{}>".format(
            self.startframe,
            self.peaks[:3],
            '...' if len(self.peaks) > 3 else '')

class Guide:
    CONTINUED = collections.namedtuple('Continued', 'guide index distance')
    KILLED = collections.namedtuple('Killed', 'guide')
    SLEEPING = collections.namedtuple('Sleeping', 'guide')

    def __init__(self, startframe, startpeak):
        self.startframe = startframe
        self.peaks = [startpeak]
        self.slept = 0
        self.frequency = startpeak.frequency

    def sleep(self):
        self.slept += 1
        self.peaks.append(Peak.zero())

    def wake(self):
        self.slept = 0

    def update(self, nextPeak, peakContribution):
        m = ((peakContribution * (nextPeak.frequency - self.frequency)) +
             self.frequency)
        self.frequency = m
        self.peaks.append(nextPeak)

    def createSearch(self, peaks, maxPeakDeviation):
        def search():
            m = self.peaks[-1].frequency
            candidates = []
            for index, peak in enumerate(peaks):
                distance = abs(peak.frequency - m)
                if distance < maxPeakDeviation:
                    candidates.append((index, distance))
            for res in sorted(candidates, key=lambda x: x[1]):
                # sorted by distance
                yield res  # we need a generator
        self._currentSearch = search()

    def advance(self, claimed, maxPeakDeviation):
        try:
            index, d = next(self._currentSearch)
            if index in claimed:
                otherGuide, otherD = claimed[index]
                if d >= otherD:  # we lost - search again
                    # works by mutating the search iterator
                    for result in self.advance(claimed, maxPeakDeviation):
                        yield result
                else:  # we won - have the other one search again
                    for result in otherGuide.advance(claimed, maxPeakDeviation):
                        yield result
                    yield self.continued(index, d)
            else:  # no conflicts - update
                yield self.continued(index, d)
        except StopIteration:  # out of search results - die
            yield self.killed()

    def toTrajectory(self, noFrames, reverse_detection):
        if reverse_detection:
            # Put the zero-peak at the beginning
            peaks = list(reversed(self.peaks[1:] + [self.peaks[0]]))
            # Change the start frame to the frame before the last frame in
            # this guide
            return Trajectory(
                noFrames - (self.startframe + len(self.peaks) + 1),
                peaks)
        else:
            return Trajectory(self.startframe, self.peaks)

    @property
    def sleeping(self):
        return bool(self.slept)

    def continued(self, index, distance):
        return Guide.CONTINUED(self, index, distance)

    def killed(self):
        return Guide.KILLED(self)

    def sleeping(self):
        return Guide.SLEEPING(self)

    def __len__(self):
        return len(self.peaks)

    def __repr__(self):
        return "<Guide startframe: {} peaks: {}{}>".format(
            self.startframe,
            self.peaks[:3],
            '...' if len(self.peaks) > 3 else '')


def advanceGuides(peaks, guides, maxPeakDeviation,
                  peakContribution):
    for guide in guides:
        guide.createSearch(peaks, maxPeakDeviation)

    claimed = {}  # index: guide, distance
    killed = []
    sleeping = []

    for guide in guides:
        for result in guide.advance(claimed, maxPeakDeviation):
            if isinstance(result, Guide.KILLED):
                killed.append(result.guide)
                # in case the guide claimed one before:
                index = [i for i, (g, d) in claimed.items() if g == guide]
                if index:
                    del claimed[index]
            elif isinstance(result, Guide.SLEEPING):
                sleeping.append(result.guide)
            else:
                claimed[result.index] = result.guide, result.distance

    usedPeaks = set()
    continued = []

    for index, (guide, _) in claimed.items():
        guide.update(peaks[index], peakContribution)
        if guide.sleeping:
            guide.wake()
            # TODO: fill in gaps
        continued.append(guide)
        usedPeaks.add(index)
    # TODO: fill in gaps
    #for guide in killed:
    #    pass
    return continued + sleeping, killed, usedPeaks


def peakContinuation(frames, guides, maxPeakDeviation, peakContribution,
                     reverse_detection=True):
    print("\tDEBUG: Continuation in reverse", reverse_detection)
    if reverse_detection:
        frames = list(reversed(frames))
    # yields trajectories as the guides die; at the end, yields remaining
    # Start continuation from first frame in order to avoid trajectories
    # starting at frame -1
    for frameNo, peaks in enumerate(frames[1:]):
        guides, killed, usedPeaks = advanceGuides(
            peaks, guides, maxPeakDeviation,
            peakContribution)
        for guide in killed:
            yield guide.toTrajectory(len(frames), reverse_detection)
        if len(usedPeaks) < len(peaks):
            candidates = []
            for index, peak in enumerate(peaks):
                if index not in usedPeaks:
                    candidates.append((index, peak))
            for index, peak in candidates:
                guide = Guide(frameNo - 1, Peak.zero())
                guide.update(peak, peakContribution)
                guides.append(guide)
    for guide in guides:
        yield guide.toTrajectory(len(frames), reverse_detection)
