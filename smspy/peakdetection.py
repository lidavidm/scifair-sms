#!/usr/bin/env python3

import sys
import math
import numpy
import collections
import scipy.io.wavfile

from .util import mag, magV, mag2dB
from .stft import STFT

class Peak(collections.namedtuple('Peak', 'magnitude frequency phase')):
    def toTuple(self):
        return (self.magnitude, self.frequency, self.phase)

    @classmethod
    def zero(cls):
        return Peak(0, 0, 0)

    @classmethod
    def interpolated(cls, xDb, xComplex, kPeak, fs, n, wt):
        """
        Performs interpolation on a detected peak.

        :param xDb: One frame of the STFT data converted to dB form.
        :param xComplex: The original, complex-valued FFT data.
        :param fs: The sampling rate of the original audio.
        :param n: The size of the STFT.
        :param wt: The value of the window transform at time 0.
        :returns: (magnitude, frequency, phase)
        """
        magScale = 2 / wt
        y = lambda alpha, beta, gamma, p: beta - 0.25 * (alpha - gamma) * p
        alpha = xDb[kPeak - 1]
        beta  = xDb[kPeak]
        gamma = xDb[kPeak + 1]

        p = 0.5 * ((alpha - gamma) / (alpha - (2 * beta) + gamma))
        kstar = kPeak + p
        frequency = (fs * (kstar / n) * 2 * math.pi)  # % (2 * math.pi)

        real, imag = zip(*[(abs(mag2dB(x.real)), abs(mag2dB(x.imag)))
                           for x in (xComplex[kPeak - 1],
                                     xComplex[kPeak],
                                     xComplex[kPeak + 1])])
        real, imag = y(p=p, *real), y(p=p, *imag)
        phase = math.atan2(imag, real)
        magnitude = math.hypot(real, imag) * magScale

        return Peak(magnitude, frequency, phase)

def equalLoudness(index, fs, n, dB):
    f = index * (fs / n)
    x = .05 + (4000 / f)
    return x * (10 ** (-x))

def detectPeaks(stft, fs, flh=20, fhh=16000, **options):
    #xDb = numpy.copy(stft.magnitudeSpectra)
    xDb = stft.dBSpectra
    #for frame in xDb:
    #    for index, dB in enumerate(frame):
    #        frame[index] = equalLoudness(index, stft.fs, stft.size, dB)
    min_peak_height = options.get('min_peak_height', 0)
    general_dB_min = options.get('general_dB_range', -70)
    general_dB_max = xDb.max()
    general_dB_range = general_dB_max + general_dB_min
    local_dB_min = options.get('local_dB_range', -60)

    interpolate_peaks = options.get('interpolate_detected_peaks', False)
    print("\tDEBUG: Peak interpolation", interpolate_peaks)
    n = len(xDb[0])
    kl = round(flh * stft.size / fs)
    kh = round(fhh * stft.size / fs)

    def peakHeight(frame, kPeak, minl, minr):
        return frame[kPeak] - (0.5 * (frame[minl] + frame[minr]))

    def findPeaks(x):
        dB_max = x.max()
        dB_min = min([dB_max + general_dB_min, general_dB_range])

        def consumeToMin(k):
            current = x[k]
            k += 1
            while k < n and current > x[k]:
                current = x[k]
                k += 1
            return k - 1

        def consumeToMax(k):
            current = x[k]
            k += 1
            while k < n and current < x[k]:
                current = x[k]
                k += 1
            return k - 1

        k = kl + 1  # 1
        while k < kh - 3:  #len(x) - 3:
            try:
                k = kMin = consumeToMin(k)
                k = kPeak = consumeToMax(k)
                k = kMin2 = consumeToMin(k)
                if kMin == kMin2:
                    import pdb; pdb.set_trace();
                if x[kPeak] > dB_min:
                    yield (kMin, kPeak, kMin2)
            except IndexError:  # no more data
                return

    magScale = 2 / stft._windowTransform
    for frameIndex, (frame, spectra) in enumerate(zip(xDb, stft.frames)):
        peaks = []
        for left, peak, right in findPeaks(frame):
            height = peakHeight(frame, peak, left, right)
            if height > min_peak_height:
                if interpolate_peaks:
                    peak = Peak.interpolated(frame, spectra, peak,
                                             stft.fs, stft.size,
                                             stft._windowTransform)
                else:
                    peak = Peak(
                        stft.magnitudeSpectra[frameIndex][peak] * magScale,
                        peak * stft.ff * 2 * math.pi,
                        stft.phaseSpectra[frameIndex][peak])
                peaks.append(peak)
        yield peaks


def main():
    #fs, data = scipy.io.wavfile.read(sys.argv[1])
    #print('Sampling Frequency:', fs)
    #stft = STFT(data, fs)
    #stft.compute(numpy.hamming, 0.050, 0.020)
    stft = STFT.load('pd_precompute.npz')
    fs = stft.fs
    for peaks in peakDetection(stft, fs):
        for peak in peaks:
            print("Peak: magnitude {} frequency {} phase {}".format(*peak))
        print()
        sys.exit()

def precompute():
    fs, data = scipy.io.wavfile.read(sys.argv[1])
    print('Sampling Frequency:', fs)
    stft = STFT(data, fs)
    stft.compute(numpy.hamming, 0.050, 0.020)
    stft.save('pd_precompute')

if __name__ == '__main__':
    main()
