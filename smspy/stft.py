import numpy
import math
from . import util

def nextPowerOfTwo(x):
    """
    Find the least power of 2 greater than or equal to x.

    Taken from
    http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
    (public domain).
    """
    v = x - 1
    v |= v >> 1
    v |= v >> 2
    v |= v >> 4
    v |= v >> 16
    return v + 1

class STFT:
    def __init__(self, audioData, fs):
        self._audioData = audioData
        self._fs = fs
        self._spectra = None
        self._magSpectra = None
        self._phaseSpectra = None
        self._dbSpectra = None
        self._size = None
        self._audioFrames = len(audioData)

    def compute(self, window, framesize, hop, padding=False):
        """
        Compute the short-time Fourier Transform (STFT).

        :param window: A window function.
        :param fs: The sampling rate.
        :param framesize: The framesize (in seconds!)
        :param hop: The hop size (in seconds!)
        """
        print("\tDEBUG: STFT padding", padding)
        x = self._audioData
        samples = len(x)
        framesamp = int(framesize * self._fs)

        if padding:
            framepad = nextPowerOfTwo(framesamp)
        else:
            framepad = None

        hopsamp = int(hop * self._fs)
        self._size = hopsamp  # framesize?
        w = window(framesamp)
        self._windowTransform = sum(w)
        # need to zero-pad window/framesamp - this length is the FFT size
        ff = self._fs / framesamp
        self._ff = ff
        self._frames = numpy.array(
            [numpy.fft.fft(w * x[i:i + framesamp], framepad)
             for i in range(0, samples - framesamp, hopsamp)])

    def save(self, name):
        numpy.savez(name, **{
            'audioData': self._audioData,
            'stft':      self._frames,
            'magnitude': self.magnitudeSpectra,
            'dB':        self.dBSpectra,
            'phase':     self.phaseSpectra,
            'data':      numpy.array([self._windowTransform]),
            'header':    numpy.array([self._fs, self._ff, self._size,
                                          self._audioFrames], numpy.int32)})

    @classmethod
    def load(cls, name):
        data = numpy.load(name)
        instance = cls(data['audioData'], data['header'][0])
        instance._frames = data['stft']
        instance._ff, instance._size, instance._audioFrames = data['header'][1:]
        instance._magSpectra = data['magnitude']
        instance._phaseSpectra = data['phase']
        instance._dbSpectra = data['dB']
        instance._windowTransform = data['data'][0]
        return instance

    @property
    def fundamentalFrequency(self):
        return self._ff

    ff = fundamentalFrequency

    @property
    def samplingRate(self):
        return self._fs

    fs = samplingRate

    @property
    def size(self):
        return self._size

    @property
    def audioFrames(self):
        return self._audioFrames

    @property
    def frames(self):
        return self._frames

    @property
    def magnitudeSpectra(self):
        if self._magSpectra is None:
            self._magSpectra = util.magV(self._frames)
        return self._magSpectra

    @property
    def phaseSpectra(self):
        if self._phaseSpectra is None:
            phase = numpy.vectorize(lambda c: math.atan2(c.imag, c.real))
            self._phaseSpectra = phase(self._frames)
        return self._phaseSpectra

    @property
    def dBSpectra(self):
        if self._dbSpectra is None:
            self._dbSpectra = numpy.vectorize(util.mag2dB)(self.magnitudeSpectra)
        return self._dbSpectra

def istft(X, fs, T, hop):
    x = numpy.zeros(T*fs)
    framesamp = X.shape[1]
    hopsamp = int(hop*fs)
    for n,i in enumerate(range(0, len(x)-framesamp, hopsamp)):
        x[i:i+framesamp] += numpy.real(numpy.ifft(X[n]))
    return x
