import math

from .peakdetection import Peak


def synthesize(trajectories, stft):
    ff, fs, n = stft.ff, int(stft.fs), int(stft.size)
    T = stft.audioFrames / fs

    frames = []
    print("\tSynthesizing frames ({} total)".format(stft.audioFrames), end='')
    for L in range(0, stft.audioFrames, n):
        print(L, end=' ')

        presentTrajs = [t for t in trajectories if presentAtFrame(t, L, n)]

        for m in range(n):
            #print(m, end=' ')

            frame = 0

            for traj in presentTrajs:
                index = (L - traj.startframe) // n
                peak = traj.peaks[index]
                prevPeak = Peak.zero() if (index == 0) else traj.peaks[index - 1]
                a = magnitude(m, n, prevPeak, peak)
                p = phase(m, n, prevPeak, peak)
                #print((p * m) % (2*math.pi), math.cos(p*m))
                frame += a * math.cos(m * p)

            frames.append(frame)

    print()
    return frames


def presentAtFrame(trajectory, frame, n):
    return ((frame >= trajectory.startframe) and
            ((trajectory.startframe + (len(trajectory.peaks) * n))) > frame)

def magnitude(m, n, prevPeak, peak):
    return prevPeak.magnitude + (m * (peak.magnitude - prevPeak.magnitude) / n)

def phase(m, n, prevPeak, peak):
    deltaFreq = (peak.frequency - prevPeak.frequency) / n
    deltaFreq /= 2 * math.pi
    freq = prevPeak.frequency / (2 * math.pi)
    return (freq + (m * deltaFreq))