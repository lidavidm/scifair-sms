import math
import numpy


mag     = numpy.linalg.norm
magV    = numpy.vectorize(numpy.linalg.norm)
mag2dB  = lambda k: 20 * math.log10(abs(k))
