#!/bin/sh

echo "Making directory" $1
mkdir $1
echo "Precomputing STFT data in" "$1/stft.npz"
python3 analyze.py precompute $2 "$1/stft.npz"
echo "Finding peaks in" "$1/peaks.npz"
python3 analyze.py peaks "$1/stft.npz" "$1/peaks.npz"
echo "Creating trajectories in" "$1/trajs.npz"
python3 analyze.py continuation "$1/peaks.npz" "$1/trajs.npz"
echo "Synthesizing in" "$1/synth.wav"
python3 analyze.py synthesis "$1/trajs.npz" "$1/stft.npz" "$1/synth.wav"
